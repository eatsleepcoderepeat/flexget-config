# 0.9.0
- Added missing tv-torrent-seasons-backfill template
- Added Series-Sync-DB task and scheduled it to run 3 times weekly with other maintenance tasks to keep DB synced with manually downloaded episodes etc...
- Added tasks to configure series from trakt when importing these lists in case they haven't been seen yet so that the series begin can be properly set in these circumstances
- Added version check once weekly (appears in log) to notify us when Flexget updates are available
- Streamlined and adjusted schedules
- Moved download-seasons-toggle into scheduler functionality (where it should have been in the first place)
- Fixed issue with tv-search-seasons template improperly searching for episodes instead of seasons
- Fixed series_acquire_backfill list within the tv-remove-queued template to properly match titles (entries weren't being matched because the title hadn't been properly scrubbed)
- Abandoned attempt to improve efficiency of discovers by using a single discover, adding entries to a list and then filtering that list successively as these list operations ended up being slower than simply repeating the discovers when discovering many episodes at once (database more than quadrupled in size from a single run and so had unresolvable scaling issues)

# 0.8.2
- Fixed downloading of bad-downloads if already watched
- Fixed unintended removal of base directories such as the !bad-downloads folders during clean up operations

# 0.8.1
- Added domain_delay for torrentz.io to try to alleviate the errors our discovers are receiving from the torrentz search plugin
- Moved reused Deluge labels into secrets.yml for better configurability
- Modified the way the Purge functionality is implemented in order to fix the apscheduler error: apscheduler.scheduler Execution of job skipped: maximum number of running instances reached (1)
- Improved series/movies matching in \*-Purge-\* operations by adding a manipulate directive

# 0.8.0
- Added changelog.md
- Updated readme.md
