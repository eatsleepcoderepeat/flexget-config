# Flexget Config
This is my personal [Flexget](https://flexget.com) configuration. It is tuned for high quality, efficient, low file size downloads preferring H265/HEVC encoded content, but can be configured to your personal tastes. This configuration is being used to enable offline access for my paid-for content from on-demand subscription services.

*Please do not use this configuration to illegally download content that you have not paid for!*

# Features
- Automated downloading of series/movies
- Trakt integration
- Examines existing movies collection and downloads upgrades
- Sorts and renames downloaded series/movies to keep everything organized
- Removes older/lower quality versions when it downloads upgrades
- Cleans up unwanted files (samples, trailers, nfo, txt etc...)
- Purging of slow downloads (after checking for a live internet connection) from Deluge
- Switches to "backfill mode" to download older unwatched (according to Trakt) episodes when needed
- Removes fully watched and ended/canceled series from your Trakt acquire list
- Allows for downloading of season packs (or not) with a simple schedule change
- Allows for "bad downloads" (garbage videos, burnt in subtitles, etc...) to be removed and re-added to the download queue *simple but manual process*

## This configuration is for and has been tested in the following environment
- [unRAID](https://unraid.net/) 6.7.0
- Docker: [PlexMediaServer](https://www.plex.tv/) *by limetech* (official)
- Docker: [delugevpn-flexget-dev](https://github.com/paulpoco/docker-templates/blob/master/paulpoco/delugevpn-flexget-dev.xml) *by [paulpoco](https://github.com/paulpoco/docker-templates/tree/master/paulpoco)* (updated manually to Flexget version 2.20.27)
- [Trakt.tv](http://trakt.tv) *including [PLEX scrobbler](https://github.com/trakt/Plex-Trakt-Scrobbler)*

### To use the *bad downloads* feature
- Create a `!bad-downloads` *default, can be changed* folder within the `/media/movies/` or `/media/series/` *can be configured* folders, and move the unwanted series/movies to these folders respectively
- Example endpoint: `/media/movies/!bad-downloads/Example Movie (1900)/Example Movie (1900) - 720P.mkv`
- Flexget takes care of the rest, including removing the offending download and then acquiring a different version

### Steps to update delugevpn-flexget-dev to the current Flexget release
1. Open a console for the **delugevpn-flexget-dev** docker
2. Type in the following commands:
	- `flexget daemon stop`
	- `pip install --upgrade pip`
	- `pip install --upgrade setuptools`
	- `pip install --upgrade flexget`
	- `pip install --upgrade deluge-client`
	- `flexget daemon start -d`

### Steps for first setup
1. Create a [Trakt](http://trakt.tv) account if you don't have one yet, then create two lists; one for acquiring unwatched content, and another for acquiring even if watched (named whatever you want) *optional, but optimal*
2. Edit the secrets.yml to suit your set up including folder structure, deluge and trakt credentials, etc...
3. Add the `config.yml` and `secrets.yml` files to the flexget configuration folder
4. Open a console for the **delugevpn-flexget-dev** docker and type in the following commands:
	- `flexget trakt auth <trakt-account-name-goes-here>`
	- `flexget execute --tasks *-Sort-All` *Optional: this may take a while to complete if you have a lot of existing downloads, not recommended if simply resetting the database*
	- `flexget execute --tasks Series-List-*`
	- `flexget execute --tasks Series-Sync-DB`
	- `flexget execute --tasks *-Seed-DB --disable-tracking --learn`
	- `flexget daemon start -d` OR `/home/nobody/flexget.sh`
5. Install Trakt scrobbler plugin(s) to keep track of watched content *optional* [Plex](https://github.com/trakt/Plex-Trakt-Scrobbler), [Netflix](https://chrome.google.com/webstore/detail/traktflix/ehlckfimahifadnbecobagimllmbdmde), [Crunchyroll](https://chrome.google.com/webstore/detail/scrobbler-for-crunchyroll/ankfnfbhggdblgnengpbbpgbndebpfdi)

### Other useful console commands
- Check for errors in config syntax: `flexget check`
- Reload config: `flexget daemon reload-config`
- Dry run a task: `flexget --test execute --tasks {taskname}`
- Run a task: `flexget execute --tasks <taskname>`
- Reset DB: `flexget database reset --sure`
- Clean DB: `flexget database cleanup`
- Seed series DB: `flexget execute --tasks Seed-Series-DB --disable-tracking --learn`
- Seed movies DB: `flexget execute --tasks Seed-Movies-DB --disable-tracking --learn`
- Forget a series or episode: `flexget series forget "Example Series" "S02E06"`
- Forget a seen entry: `flexget seen forget "Example Series" "S02E06"`
- Get available search plugins: `flexget plugins --interface search`
- Get documentation for a plugin: `flexget doc <plugin-name>`
- Specific to this docker environment (arch linux)
	- Start Flexget: `/home/nobody/flexget.sh`
	- See system resources: `htop`
	- Completely reset flexget and start from scratch
		- `flexget daemon stop` OR restart docker to terminate immediately (killall and pkill don't seem to work)
		- `flexget database reset --sure` OR `rm -f <path-to-flexgetdev-folder>.sqlite` in unRAID console - example: `rm -f /mnt/user/appdata/flexgetdev/.sqlite`
		- **Step 4** from **Steps for first setup** above

### Deluge configuration
For best efficiency, enable the `Do not download slow torrents` option within the **Prefernces>Queue** menu

### Note
Linux commands used throughout may be inefficient - I have only just started to learn linux commands beginning with this project

### To Do
- Set up pushbullet notifications
- Set up automated download of subtitles
- Learn how to use YAML anchors to clean up our code (especially where repeated) - ref: https://medium.com/@kinghuang/docker-compose-anchors-aliases-extensions-a1e4105d70bd
- Code optimization - move repeated code (like Jinja filters) into variables or YAML anchors (not sure which is feasiable yet, needs testing)
- Clean up templates that aren't really necessary and move into tasks
- Figure out why trakt.persist is returning movies that aren't displaying in this list from Trakt.tv, for the Movies-List-Trakt-Remove-Duplicates task *I believe this is a caching related issue*
- Automatic restart of Flexget daemon after a crash

### Attribution
Some code snippets reused from the [Lazy Couch Experience](https://github.com/zilexa/Flexget-the-lazy-couch-experience) - particularly the reject-these, disable-seen-retry and series/movies-metainfo templates - **THANK YOU!**

### [Changelog](CHANGELOG.md)
